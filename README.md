ansible-pex
===========

Repository to build Ansible [PEX] files.

[PEX] files are self-contained executable Python virtual environments that
make it easy to deploy Python applications.

The PEX files are built by GitLab CI when pushing a tag.
The Ansible version to build shall be updated in the .gitlab-ci.yml file.


License
-------

BSD 2-clause


[PEX]: https://pex.readthedocs.io/en/stable/index.html
